package OPCUA

import (
	"context"
	"errors"
	"fmt"
	"github.com/gopcua/opcua"
	"github.com/gopcua/opcua/id"
	"github.com/gopcua/opcua/ua"
	"log"
	"strings"
	"time"
)

const (
	UserTokenAnonymous = "Anonymous"
	UserTokenUserName = "UserName"
	UserTokenCertificate = "Certificate"
	UserTokenIssuedToken = "IssuedToken"
)
type ClientSubscribeOptions struct {
	Interval             time.Duration
	Namespace            uint16
}

type DataPackage struct {
	Timestamp time.Time
	Value     interface{}
	Path      string
	Status    ua.StatusCode
}

type Client interface {
	ConnectToServer() error
	Disconnect() error
	SubscribeNodes(nodePaths []string, opts ClientSubscribeOptions) error
	ListenData(targetChannel chan DataPackage)
}

type client struct {
	client            *opcua.Client
	address           string
	rootNode          *opcua.Node
	context           context.Context
	subscriptions     []*opcua.Subscription
	messageChannel    chan *opcua.PublishNotificationData
	monitoringHandles map[uint32]string
	endpoint          *ua.EndpointDescription
}

// ConnectToServer creates the connection to the server and returns possible error
func (c *client) ConnectToServer() error {
	if err := c.client.Connect(c.context); err != nil {
		return err
	}

	log.Println("Connected to client: ", c.address)
	nid, err := ua.ParseNodeID(fmt.Sprintf("i=%d", id.RootFolder))
	if err != nil {
		return err
	}

	c.rootNode = c.client.Node(nid)
	return nil
}

// Disconnect closes the connection and returns possible error
func (c *client) Disconnect() error {
	return c.client.Close()
}


// SubscribeNodes takes list of paths (relative to Objects directory) and ClientSubscribeOptions structure and starts
// monitoring the given nodes. If some node is not found, then it is logged and ignored.
func (c *client) SubscribeNodes(nodePaths []string, opts ClientSubscribeOptions) error {
	//namespace := opts.Namespace
	interval := opts.Interval

	c.messageChannel = make(chan *opcua.PublishNotificationData)

	objectsNodeId, err := ua.ParseNodeID(fmt.Sprintf("i=%d", id.ObjectsFolder))
	if err != nil {
		return err
	}
	objectsNode := c.client.Node(objectsNodeId)
	sub, err := c.client.Subscribe(&opcua.SubscriptionParameters{
		Interval: interval,
	}, c.messageChannel)
	if err != nil {
		log.Println("Failed to create subscription...")
		return err
	} else {
		c.subscriptions = append(c.subscriptions, sub)
	}
	var monitoringRequests []*ua.MonitoredItemCreateRequest
	for i, node := range nodePaths {
		path := strings.Replace(node, "/", ".", -1)
		nid, err := objectsNode.TranslateBrowsePathInNamespaceToNodeID(2, path)
		if err != nil {
			log.Println("Could not find node ", path, err)
			continue
		}
		clientId := uint32(5000 + i)
		monitoringRequests = append(
			monitoringRequests,
			opcua.NewMonitoredItemCreateRequestWithDefaults(nid, ua.AttributeIDValue, clientId))
		c.monitoringHandles[clientId] = node
	}
	if len(monitoringRequests) == 0 {
		return errors.New("no monitoring requests")
	}
	res, err := sub.Monitor(ua.TimestampsToReturnBoth, monitoringRequests...)
	if err != nil || (res != nil && res.Results[0].StatusCode != ua.StatusOK) {
		log.Fatal(err, res.Results[0].StatusCode)
	}
	go sub.Run(c.context)

	return nil
}

// Start listening data from the message channel and forward the relevant information to the given targetChannel.
// If there is a case where the value reading fails (panics), the item is relayed to the error channel from which
// it can be read and debugged. This function runs in its own go process.
func (c *client) ListenData(targetChannel chan DataPackage, errorChannel chan interface{}) {
	go func() {
		for {
			select {
			case <-c.context.Done():
				return
			case res := <-c.messageChannel:
				if res.Error != nil {
					if res.Error != ua.StatusBadSequenceNumberUnknown {
						log.Println("ERROR: ", res.Error)
					}
					continue
				}
				switch x := res.Value.(type) {
				case *ua.DataChangeNotification:
					for _, item := range x.MonitoredItems {
						func() {
							defer func() {
								if r := recover(); r != nil {
									fmt.Printf("Encountered an error with item %v, value %v\n", item, item.Value)
									errorChannel <- item
								}
							}()
							data := item.Value.Value.Value()
							pkg := DataPackage{
								Value:     data,
								Path:      c.monitoringHandles[item.ClientHandle],
								Timestamp: item.Value.SourceTimestamp,
								Status:    item.Value.Status,
							}
							targetChannel <- pkg
						}()
					}
				case *ua.EventNotificationList:
				default:
					log.Printf("Unknown publish: %T", res.Value)
				}
			}
		}
	}()
}

// NewOPCUAClient creates and returns a new client for opc ua. It does not yet create the connection and calling
// ConnectToServer is required. The security policies are defined under constants at
// https://godoc.org/github.com/gopcua/opcua/ua#pkg-variables . The message security mode is either "Invalid", "None",
// "Sign" or "SignAndEncrypt".
func NewOPCUAClient(address string, securityPolicy string,
	messageSecurityMode string, userTokenType string, opts ...opcua.Option) (*client, error) {

	uaMessageSecurityMode := ua.MessageSecurityModeFromString(messageSecurityMode)
	endpoints, endpointError := opcua.GetEndpoints(address)
	if endpointError != nil {
		return nil, endpointError
	}

	endpointDescription := opcua.SelectEndpoint(endpoints, securityPolicy, uaMessageSecurityMode)
	opts = append(opts, opcua.SecurityFromEndpoint(endpointDescription, ua.UserTokenTypeFromString(userTokenType)))
	return &client{
		client:            opcua.NewClient(address, opts...),
		context:           context.Background(),
		address:           address,
		monitoringHandles: make(map[uint32]string),
		endpoint:          endpointDescription,
	}, nil
}

// ListEndpoints is a helper function intended for development in case there is uncertainty about the server and its
// endpoints.
func ListEndpoints(address string) error {
	endpoints, err := opcua.GetEndpoints(address)
	if err != nil {
		return err
	}
	for _, endpoint := range endpoints {
		fmt.Printf("EndpointURL: %s, SecurityPolicyURI: %s, SecurityMode: %v\n",
			endpoint.EndpointURL, endpoint.SecurityPolicyURI, endpoint.SecurityMode)
	}
	return nil
}


