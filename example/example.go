package main

import (
	"fmt"
	easyua "gitlab.com/blissfulreboot/golang/easier-opcua"
	"github.com/gopcua/opcua"
	"os"
	"time"
)

// Simple example on how to use the library. This has been implemented using Microsoft's OPC UA server
// (https://hub.docker.com/_/microsoft-iotedge-opc-plc)
func main() {
	easyua.ListEndpoints("opc.tcp://localhost:50000")
	opts := opcua.AuthUsername("user1", "password")
	client, _ := easyua.NewOPCUAClient(
		"opc.tcp://localhost:50000",
		"None",
		"None",
		easyua.UserTokenUserName,
		opts)
	client.ConnectToServer()
	defer client.Disconnect()
	targetChannel := make(chan easyua.DataPackage, 100000)
	errorChannel := make(chan interface{}, 100000)
	subOpts := easyua.ClientSubscribeOptions{
		Interval:             time.Duration(100) * time.Millisecond,
		Namespace:            2,
	}
	nodes := []string{
		"OpcPlc/Telemetry/FastDouble1",
		"OpcPlc/Telemetry/FastDouble2",
		"OpcPlc/Telemetry/FastDouble3",
		"OpcPlc/Telemetry/FastDouble4",
		"OpcPlc/Telemetry/FastDouble5",
		"OpcPlc/Telemetry/AlternatingBoolean",
		"OpcPlc/Telemetry/DipData",
		"OpcPlc/Telemetry/NegativeTrendData",
		"OpcPlc/Telemetry/PositiveTrendData",
		"OpcPlc/Telemetry/RandomUnsignedInt32",
		"OpcPlc/Telemetry/SpikeData",
	}

	if err := client.SubscribeNodes(nodes, subOpts); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	client.ListenData(targetChannel, errorChannel)

	go func() {
		for {
			select {
			case msg := <-targetChannel:
				fmt.Printf("%+v\n", msg)
			case msg := <-errorChannel:
				fmt.Printf("Error: %+v", msg)
			}
		}
	}()
	select {}
}
